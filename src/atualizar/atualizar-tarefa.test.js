import React from 'react';
import ReactDOM from 'react-dom';
import AtualizarTarefa from './atualizar-tarefa';

describe('Atualizar cadastro', ()=>{
    it('Atualizar sem erros', () => {
        const div = document.createElement('div');
        ReactDOM.render(<AtualizarTarefa id={1}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});