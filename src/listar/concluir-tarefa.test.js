import React from 'react';
import ReactDOM from 'react-dom';
import ConcluirTarefa from './concluir-tarefa';
import Tarefa from '../models/tarefa.model';
import { render, fireEvent, getByTestId } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('Teste do componente de conclusão de tarefas', () =>{

    const nomeTarefa = 'TArefa de teste';
    const tarefa = new Tarefa (1, nomeTarefa, false);

    it('Deve renderizar sem erro', () =>{
        const div = document.createElement('div');
        ReactDOM.render(<ConcluirTarefa tarefa={tarefa} recarregarTarefas={() => false} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('Deve exibir a Modal', () => {
        const { getByTestId } = render (<ConcluirTarefa tarefa={tarefa} recarregarTarefas={() => false} />);
        fireEvent.click(getByTestId('btn-abrir-modal'));
        expect(getByTestId('modal')).toHaveTextContent(nomeTarefa);
    });

    it('Deve concluir uma tarefa', () => {

        localStorage['tarefas'] = JSON.stringify([tarefa]);
        const { getByTestId } = render (<ConcluirTarefa tarefa={tarefa} recarregarTarefas={() => false} />);

        fireEvent.click(getByTestId('btn-abrir-modal'));
        fireEvent.click(getByTestId('btn-concluir'));

        const tarefasDb = JSON.parse(localStorage['tarefas']);

        expect(tarefasDb[0].concluida).toBeTruthy();
    });

});