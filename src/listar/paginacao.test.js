import React from 'react';
import ReactDOM from 'react-dom';
import Paginacao from './paginacao';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('Teste do componente paginação', () => {
    it('Deve renderizar o componente sem erros', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Paginacao totalItems={10} 
                                    paginaAtual={10}
                                    temsPorPagina={10} 
                                    mudarPagina={() => false} />, div)
        ReactDOM.unmountComponentAtNode(div);
    });
});